import type { AxiosPromise } from 'axios';
import { apiInstance } from './base';
import type { Todo } from './models';

const BASE_URL = '/';

export type GetTodosListParams = {
  id?: number;
  completed?: boolean;
};
export const getTodosList = (
  params?: GetTodosListParams,
): AxiosPromise<Todo[]> => apiInstance.get(BASE_URL, { params });

export type GetTodoByIdParams = {
  todoId: number;
};

export const getTodoById = ({
  todoId,
  ...params
}: GetTodoByIdParams): AxiosPromise<Todo> => apiInstance.get(`${BASE_URL}/${todoId}`, { params });
