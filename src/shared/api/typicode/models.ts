enum Tags {
  STUDY = 'учёба',
  PERSONAL = 'личное',
  PLANS = 'планы',
}

export type Todo = {
  id: number;
  task: string;
  tag: Tags;
  completed: boolean;
};
