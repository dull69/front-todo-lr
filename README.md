# Запустить проект

без докера

```sh
npm i
npm run dev
```

с докером

```sh
sudo docker build -t front-todo-lr .
sudo docker run -it -p 8080:8080 --rm front-todo-lr
```
